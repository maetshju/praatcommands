'''
praatcommands.py

Copyright 2017 Matthew C. Kelley
Developed at the University of Alberta

 This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License, version 2.1,
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

from __future__ import print_function
import tempfile
import subprocess
import time

class PraatCommands:

    '''Represents a sequence of Praat scripting commands to send to the Praat
    program. Both command-line and GUI scripting commands are supported, though
    the sendpraat program is required to be able to use the GUI scripting. When
    looping, the best performance using these tools comes from looping to add
    lines to the eventual script, and then executing, rather than looping
    through small versions of the script and executing at each pass through the
    loop.

    Args:
        cmds (list): Each of the commands to be added to the script that will be
        executed. Each element of the list will be placed on a new line when
        the script is to be executed.

        exe (str): Path to the Praat executable. This can be relative or
        absolute.

        args (str): Arguments to the script. Used when the script opens with a
        form. The passed arguments will be used as the values for the fields in
        the form.

        gui (boolean): Whether the commands are for a GUI instance of Praat or
        not.

        sendpraat (str): Path to the sendpraat executable. This can be relative
        or absolute. Only used for GUI scripts.

        newinstance (boolean): If `True`, a new GUI instance of Praat will be
        started before the commands are executed. Only used for GUI scripts.
    '''

    def __init__(self, cmds=[], exe='praat', args=[], gui=False, sendpraat=None, newinstance=True):

        self.cmds = cmds
        self.exe = exe
        self.args = args
        self.gui = gui
        self.sendpraat = sendpraat
        self.newinstance = newinstance

    def run_praat_commands(self, print_output=False):
        '''Executes Praat commands in a script.

        Args:
            print_output (boolean): If `True`, the output of the script will be
            printed as well as returned.

        Returns:
            A `list` of containing the output. Each element is a `str` of a line
            of output.
        '''

        # Write commands to temporary file to be passed to Praat later on
        with tempfile.NamedTemporaryFile(mode='w', suffix='.praat', delete=False) as t:

            tname = t.name

            for c in self.cmds:

                t.write('{}\n'.format(c))

        if self.gui:

            if self.newinstance:

                subprocess.Popen(self.exe)
                time.sleep(2)

            call_command = [self.sendpraat, 'praat',  'runScript: "{}"'.format(t.name)]
            output = subprocess.call(call_command)

        else:

            call_command = [self.exe, '--run', t.name] + self.args
            output = subprocess.check_output(call_command, universal_newlines=True)

            if print_output:
                print(output, end='')

        return output.rstrip().split('\n')

def main():

    '''Dummy function that tests basic functionality of the command-line
    version.
    '''

    praat = PraatCommands(exe='/home/matt/praat')
    praat.cmds.append('writeInfoLine: "test"')
    praat.cmds.append('appendInfoLine: "test2"')
    print(praat.run_praat_commands())

if __name__ == '__main__':
    main()
