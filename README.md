This script contains functions that allow a user to use the [Praat](http://www.fon.hum.uva.nl/praat/) program's scripting interface in Python. In essence, it is a wrapper for writing a temporary file and calling Praat to run that file like a script. It is not designed to replace Praat's scripting language, nor does it replicate the API. Rather, it just passes the commands into Praat. As such, it is necessary to be familiar with [Praat's scripting functionality](http://www.fon.hum.uva.nl/praat/manual/Scripting.html) to be able to use these functions effectively.

As a note, if you are using some kind of loop with this script with looping functions or similar in Python, try to use the loops to append all the commands to be run into the commands list and **then** run the commands. Doing otherwise and executing commands at every iteration in a loop leads to very slow performance because Praat is constantly being opened and closed, in addition to the overhead Python is adding to the process.

# General workflow

The general workflow for using the functions in the script is to create a `PraatCommands` object and then add commands to it. Once the commands have all been added, the `run_praat_commands()` function is called, and the commands are sent to the Praat program. The output is returned so that the user can access the the results of issuing the commands.

Some examples of using this functionality can be seen in the [Praat Cookbook](http://maetshju.gitlab.io/praat-cookbook/) I'm working on.